from django.http import request
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_bulk.generics import ListBulkCreateAPIView
from exam.models import Speciality, Topics
from exam.serializers import SpecialitySerializer, JournalSerializer, TopicsSerializer


class SpecialityAPI(APIView):
    def get(self, request, format=None):
        speciality = Speciality.objects.all()
        serilizer = SpecialitySerializer(speciality, many=True)
        return Response(serilizer.data)


class TopicsAPI(APIView):
    def get(self, request, format=None):
        id = request.GET.get('bankid')
        topics = Topics.objects.filter(speciality=id)
        serilizer = TopicsSerializer(topics, many=True)
        return Response(serilizer.data)


class Cheque(ListBulkCreateAPIView):
    """
    Retrieve, update or delete a snippet instance.
    """
    serializer_class = JournalSerializer
