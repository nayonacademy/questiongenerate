from django import forms

from exam.models import Speciality, Topics, Answer, Question, Distribution


class SpecialityForm(forms.ModelForm):
    class Meta:
        model = Speciality
        fields = '__all__'


class TopicsForm(forms.ModelForm):
    class Meta:
        model = Topics
        fields = '__all__'


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = '__all__'


class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = '__all__'


class DistributionForm(forms.ModelForm):
    class Meta:
        model = Distribution
        fields = '__all__'

