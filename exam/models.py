from __future__ import unicode_literals

from django.contrib.auth.models import User
from django.db import models

# Create your models here.


class Speciality(models.Model):
    name = models.CharField(max_length=45, blank=True)
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class Topics(models.Model):
    name = models.CharField(max_length=45, blank=True)
    speciality = models.ForeignKey(Speciality, blank=True)
    create_date = models.DateTimeField(auto_now=True)
    user = models.IntegerField(default=1)

    def __str__(self):
        return self.name


class Question(models.Model):
    name = models.CharField(max_length=45, blank=True)

    def __str__(self):
        return self.name


class Answer(models.Model):
    qstion = models.CharField(max_length=45, blank=True)
    answr = models.CharField(max_length=45, blank=True)
    question = models.ForeignKey(Question, blank=True)

    def __str__(self):
        return self.qstion


class Journal(models.Model):
    name = models.CharField(max_length=45, null=True, blank=True)
    topics = models.ForeignKey(Topics)
    create_date = models.DateTimeField(auto_now=True)
    user = models.IntegerField(default=1)

    def __str__(self):
       return str(self.pk)


class JournalTransaction(models.Model):
    qstion = models.CharField(max_length=45, blank=True)
    answr = models.CharField(max_length=45, blank=True)
    journal = models.ForeignKey(Journal, blank=True, on_delete=models.CASCADE, related_name="journal_transactions")
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
       # return 'journaltransactionstr'
        return self.qstion

class Distribution(models.Model):
    user = models.ForeignKey(User)
    speciality = models.ForeignKey(Speciality, default=1)
    create_date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.topics
