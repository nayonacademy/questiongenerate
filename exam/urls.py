from django.conf.urls import url, include

from exam import views

urlpatterns = [
    url(r'^$', views.login, name="login"),
    url(r'^logout/$', views.user_logout, name="logout"),
    url(r'^dashboard/$', views.dashboard, name="dashboard"),
    url(r'^dashboard/(?P<id>.*)/$', views.update_speciality, name="update_speciality"),
    url(r'^speciality/$', views.create_speciality, name="speciality"),
    url(r'^question/$', views.create_question, name="question"),
    url(r'^question/list$', views.question_list, name="question_list"),
    url(r'^topics/$', views.create_topic, name="topics"),
    url(r'^topics/list/$', views.topic_list, name="topics_list"),
    url(r'^create_newuser/$', views.create_newuser, name="create_newuser"),
    url(r'^teacher/list/$', views.user_list, name="teacher_list"),
    url(r'^distribution/$', views.distributionview, name="distribution"),
    url(r'^generate/$', views.generate_exam, name="generate_question"),
    url(r'^select_topics/$', views.selecttopics, name="select_topics"),
]