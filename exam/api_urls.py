from django.conf.urls import url, include
from rest_framework import routers
from . import api_views
router = routers.DefaultRouter()

urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^speciality/$', api_views.SpecialityAPI.as_view(), name='speciality_api'),
    url(r'^topics/$', api_views.TopicsAPI.as_view(), name='topics_api'),
    url(r'^cheque/receive', api_views.Cheque.as_view(), name="cheque_receive"),
]