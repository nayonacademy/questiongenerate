from django.contrib.auth import authenticate, logout
from django.contrib.auth import login as auth_login
from django.contrib.auth.decorators import login_required
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
# Create your views here.
from exam.forms import SpecialityForm, TopicsForm, DistributionForm
from exam.models import Speciality, Topics, Journal, JournalTransaction
from django.contrib import messages
from django.contrib.auth.models import User

# =========================== LOG IN & LOG OUT AREA ========================== #


def login(request):
    """
    User Login area
    :param request:
    :return: Dashboard
    """
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user:
            if user.is_active:
                auth_login(request, user)
                return HttpResponseRedirect('dashboard')
            else:
                return HttpResponse("Your account is disabled.")
        else:
            print "Invalid login details: {0}, {1}".format(username, password)
            return HttpResponse("Invalid login details supplied.")
    else:
        return render(request, 'login.html')


@login_required
def user_logout(request):
    """
     User Logout
    :param request:
    :return: Login page
    """
    logout(request)
    return HttpResponse("Successfully Logout")


# =========================== SPECIALITY ===================================== #
@login_required
def create_speciality(request):
    """
    Create Speciality
    :param request:
    :return:
    """
    if request.method == 'POST':
        form = SpecialityForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            form = SpecialityForm()
            messages.success(request, 'Form submission successful')
    else:
        form = SpecialityForm()
    return render(request, 'create_speciality.html', {'form': form})


@login_required
def dashboard(request):
    """
    Show all the Speciality as a list view
    :param request:
    :return: Speciality list
    """
    if request.user.is_superuser:
        queryset = Speciality.objects.all()
    else:
        queryset = Speciality.objects.filter(distribution__user__username=request.user.username)
    context = {
        "object_list": queryset
    }
    return render(request, 'dashboard.html', context)


def update_speciality(request, id):
    if request.method == "GET":
        speciality = Speciality.objects.get(id=id)
    if request.method == "POST":
        name = request.POST.get('name')
        print (name)
        id = request.POST.get('id')
        speciality = Speciality.objects.filter(id=id)
        speciality.name = name
        speciality.save()

    context = {
        "speciality": speciality
    }
    return render(request, 'update_speciality.html', context)


# =========================== QUESTIONS ===================================== #
@login_required
def create_question(request):
    """
    Create Questions with multiple options
    :param request:
    :return:
    """
    queryset = Topics.objects.filter(user=request.user.id)
    return render(request, 'create_question.html', {'obj_list': queryset})


@login_required
def question_list(request):
    """
    Show the questions list
    :param request:
    :return:
    """
    queryset = Journal.objects.filter(user=request.user.id)
    context = {
        "object_list": queryset
    }
    return render(request, 'list_questions.html', context)


# =========================== TOPICS ===================================== #
@login_required
def create_topic(request):
    """
    Create question under specific Speciality
    :param request:
    :return:
    """
    special = Speciality.objects.filter(distribution__user__username=request.user.username)
    if request.method == 'POST':
        form = TopicsForm(request.POST)
        if form.is_valid():
            print ("I am here One")
            instance = form.save(commit=False)
            instance.save()
            print ("I am here One")
            form = TopicsForm()
            print ("I am here One")
            messages.success(request, 'Form Submission Successful')
    else:
        form = TopicsForm()
    context = {
        "form": form,
        "special": special
    }
    return render(request, 'create_topics.html', context)


@login_required
def topic_list(request):
    """
    Show the Topics List
    :param request:
    :return:
    """
    if request.user.is_superuser:
        queryset = Topics.objects.all()
    else:
        queryset = Topics.objects.filter(user=request.user.id)
    context = {
        "object_list": queryset
    }
    return render(request, 'list_topics.html', context)


# =========================== NEW USERS ===================================== #
@login_required
def create_newuser(request):
    """
    Create New User
    :param request:
    :return:
    """
    if request.method == 'POST':
        username = request.POST.get("username", None)
        email = request.POST.get("email", None)
        password = request.POST.get("password", None)
        first_name = request.POST.get("first_name", None)
        last_name = request.POST.get("last_name", None)

        user, created = User.objects.get_or_create(username=username, email=email)
        if created:
            user.set_password(password)  # This line will hash the password
            user.first_name = first_name
            user.last_name = last_name

            user.save()  # DO NOT FORGET THIS LINE

        print (username, email, password, first_name, last_name)
    return render(request, 'create_user.html')


@login_required
def user_list(request):
    """
    Show the User List
    :param request:
    :return:
    """
    queryset = User.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, 'list_user.html', context)


# =========================== DISTRIBUTIONS ===================================== #
@login_required
def distributionview(request):
    """
    Distribute user to specific speciality
    :param request:
    :return:
    """
    form = DistributionForm()
    if request.method == 'POST':
        form = DistributionForm(request.POST)
        if form.is_valid():
            instance = form.save(commit=False)
            instance.save()
            form = DistributionForm()
            messages.success(request, 'Form submission successful')
    return render(request, 'distribution.html', {'form': form})


# =========================== QUESTIONS GENERATIONS ===================================== #
def generate_exam(request):
    """
    Admin can Generate the questions
    :param request:
    :return:
    """
    # queryset = JournalTransaction.objects.filter(journal__topics=13)
    topicsno = request.POST.get("topicsno", None)
    limitno = request.POST.get("limitno", None)
    queryset = Journal.objects.filter(topics=topicsno)[0:limitno]
    context = {
        "object_list": queryset
    }
    return render(request, 'generate_exam.html', context)


def selecttopics(request):
    """
    Generate question need select the specific category and the question number
    :param request:
    :return:
    """
    queryset = Topics.objects.all()
    context = {
        "object_list": queryset
    }
    return render(request, 'select_topics.html', context)
