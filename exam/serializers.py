from rest_framework import serializers
from rest_framework import serializers
from rest_framework_bulk.drf3.serializers import BulkSerializerMixin, BulkListSerializer
from exam.models import Speciality, Question, Answer, Journal, JournalTransaction, Topics


class ParentChildSerializerMixin(object):
    def save_the_children(self, validated_data, child_models):
        """
        This function will save the related object and then attach newly created
        object with the targeted object

        :param validated_data:
        :param child_model: a list of tupple in [(field_name, ModelClass)] format
        :return:
        """
        for field, model in child_models:
            child_data = validated_data.pop(field, None)
            if child_data:
                validated_data[field] = model.objects.create(**child_data)
        return validated_data


class SpecialitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Speciality
        fields = '__all__'


class TopicsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Topics
        fields = '__all__'


class JournalTransactionSerializer(BulkSerializerMixin, serializers.ModelSerializer):
    """
    WARNING: debit and credit fields are not present in model
    As per ModelSerializer current feature extra fields are not allowed.
    We are doing small tricks here through JournalSerialzer to work this around.
    """
    class Meta:
        model = JournalTransaction
        list_serializer_class = BulkListSerializer
        fields = '__all__'


class JournalSerializer(ParentChildSerializerMixin, BulkSerializerMixin, serializers.ModelSerializer):
    """
    Cheque voucher entry serializer
    """
    journal_transactions = JournalTransactionSerializer(many=True)

    class Meta:
        model = Journal
        list_serializer_class = BulkListSerializer
        fields = '__all__'

    def create(self, validated_data):
        """
        overriding default create method to facilitate
        bulk creation and relation

        :param validated_data:
        :return:
        """
        journal_transactions_data = validated_data.pop('journal_transactions')
        journal = Journal.objects.create(**validated_data)

        # Looping over each journal transaction posted with a single journal entry
        for transaction_data in journal_transactions_data:
            JournalTransaction.objects.create(journal=journal, **transaction_data)
        return journal